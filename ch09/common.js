

/**
 * @property {String} text
 */
function outputText(text)
{
	var body = document.getElementsByTagName('body')[0];
	var p = document.createElement('p');
	p.appendChild(document.createTextNode(text));
	body.appendChild(p);
}

